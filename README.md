# 3D-printing #

This repo is to help you print on our 3D printers in the lab at AAU

### What is this repository for? ###

* Quick start guides
* FAQ
* Code and settings for the printers
* usefull STL files for 3D-printer parts or test pieces

### Contribution guidelines ###

* Any help on filling this repo with usefull stuff is welcome
* Links to other guides are also always nice to have

### Who do I talk to? ###

* Hjalte Nielsen (main responsible for makerspace at fib14) 
* Other people are welcome on this list if they want. 